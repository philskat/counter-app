# Counter-App

This flutter app can be uesd to count stuff. Support for multiple counters is planned.

## Running the App

To run the app you have to install the Flutter CLI from the [Flutter Website](https://flutter.dev/)

Than you can run `flutter run` in the root directory of the project to start the app in development mode.
It is also possible to use VSCode or an other IDE to run the app and develop it. Vist [Flutter Documentation](https://flutter.dev/docs/get-started/editor?tab=vscode) to see the setup for your IDE.

## Build the App

To build the app you also need the Flutter CLI.

Than you can run `flutter build` to get a list of availible formats. 

### Example
```
flutter build apk
``` 
This will create an APK-File of the app that can be installed on an Android smartphone.