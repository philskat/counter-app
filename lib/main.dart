import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Counter',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        accentColor: Colors.amber,
        primaryColor: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Counter(),
    );
  }
}

class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  int _counter = 0;

  void incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void decrementCounter() {
    setState(() {
      if (_counter > 0) _counter--;
    });
  }

  void reset() {
    setState(() {
      _counter = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Scaffold(
        appBar: AppBar(
          title: Text("Counter"),
          actions: [IconButton(icon: Icon(Icons.refresh), onPressed: reset)],
        ),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: Center(
                  child: GestureDetector(
                      onTap: incrementCounter,
                      child: Text(_counter.toString(),
                          style: TextStyle(fontSize: 90)))),
            ),
            Container(
                margin: EdgeInsets.only(bottom: 8, left: 16, right: 16),
                child: SizedBox(
                    width: 1000,
                    height: 80,
                    child: RaisedButton(
                        child: Text("+",
                            style: TextStyle(
                                color: theme.colorScheme.onSecondary)),
                        color: theme.colorScheme.secondary,
                        onPressed: incrementCounter))),
            Container(
                margin: EdgeInsets.only(bottom: 16, left: 16, right: 16),
                child: SizedBox(
                    width: 1000,
                    height: 50,
                    child: OutlineButton(
                      child: Text("-"),
                      onPressed: decrementCounter,
                    )))
          ],
        ));
  }
}
